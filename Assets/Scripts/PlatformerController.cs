﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class PlatformerController : MonoBehaviour {

	[HideInInspector] bool facingRight=true;
	[HideInInspector] bool jump=true;


	public Text scoreNumber;

	public static PlatformerController pc;
	public float moveForce=365f;
	public float speed=5f;
	public float jumpForce=1000f;
	public Transform groundCheck;
	public bool movingEnabled=true;

	private bool grounded=false;
	private Animator anim;
	public Rigidbody2D rb2d;

	// Use this for initialization
	void Awake () 
	{
		anim = GetComponent<Animator> ();
		rb2d = GetComponent<Rigidbody2D> ();
		scoreNumber.text = GameManager.score.ToString ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		grounded = Physics2D.Linecast(transform.position,groundCheck.position,1<<LayerMask.NameToLayer("Ground"));

		if (Input.GetButtonDown("Jump") && grounded) 
		{
			jump = true;

		}


	}

	public void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag ("Crate")) {
			Destroy (other.gameObject);
			GameManager.instance.UpdateScore ();
			scoreNumber.text = GameManager.score.ToString ();
		}
	}



	void FixedUpdate()
	{
		float h = Input.GetAxis ("Horizontal");
		anim.SetFloat ("Speed", Mathf.Abs (h));
		if (movingEnabled) {
			if (h * rb2d.velocity.x < speed) {
				rb2d.AddForce (Vector2.right * h * moveForce);
			}
			if (Mathf.Abs (rb2d.velocity.x) > speed) {
				rb2d.velocity = new Vector2 (Mathf.Sign (rb2d.velocity.x) * speed, rb2d.velocity.y);
			}

			if (h > 0 && !facingRight)
				Flip ();
			else if (h < 0 && facingRight)
				Flip ();

			if (jump) {
				anim.SetTrigger ("Jump");
				rb2d.AddForce (new Vector2 (0f, jumpForce));
				jump = false;
			}
		}
	}

	//obrót ludka
	void Flip()
	{
		facingRight = !facingRight;
		Vector3 scale = transform.localScale;
		scale.x *= -1;
		transform.localScale = scale;
	}

}
