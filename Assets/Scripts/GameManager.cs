﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static GameManager instance;
	//public Text scoreNumber;
	public static int score=0;

	//public bool gameOver=false;

	// Use this for initialization
	void Awake () {
		
		if (instance == null) {
			DontDestroyOnLoad (gameObject);
			instance = this;
		} 
		else if (instance != this) {
			Destroy (gameObject);	
		}

	}





	public void UpdateScore()
	{	
		score++;
	}

	public void loadLevel(int level)
	{
		SceneManager.LoadScene (level);
	}




}
