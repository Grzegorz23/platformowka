﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthManager : MonoBehaviour {


	private int lifesNumber=2;
	public GameObject[] lifes;

	public PlatformerController pc;
	// Use this for initialization
	void Start () {
		
	}
	
	void checkLifes()
	{
		Debug.Log (lifesNumber);
		for(int i=0;i<lifes.Length;i++)
		{
			if(i<=lifesNumber)
				lifes[i].SetActive(true);
			else
				lifes[i].SetActive(false);
		}

	}


	public void takeLifes()
	{
		if (lifesNumber > 0) {	
			lifes[lifesNumber].SetActive(false);
			lifesNumber--;
		} 
		else 
		{
			Debug.Log ("tu bylem");
			GameManager.score = 0;
			GameManager.instance.loadLevel (0);
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{

		if(other.gameObject.CompareTag("Obstacle"))
		{
			knockBack ();
			takeLifes();
		}
	}


	private void knockBack()
	{
		StartCoroutine ("haltMovement");
		Vector2 vector = new Vector2 (-15f,20f);
		pc.rb2d.velocity = vector;

	}

	IEnumerable haltMovement(){
		pc.movingEnabled = false;
		yield return new WaitForSeconds (1.0f);
		pc.movingEnabled = true;
	}



}
