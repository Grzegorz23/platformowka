﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawner : MonoBehaviour {

	public int numberOfCoins=3;
	public GameObject coinPrefab;
	public float range = 1f;


	// Use this for initialization
	void Start () {
		spawnCoins ();
	}
	
	public void spawnCoins()
	{
		for (int i = 0; i < numberOfCoins; i++) {
			Vector2 spawnRange = new Vector2 (Random.Range(-range,range),Random.Range(0,3*range));
			//Debug.Log ((Vector2)transform.position);
			//Debug.Log ((Vector2)transform.position+spawnRange +"pozycja");
			Instantiate (coinPrefab,(Vector2)transform.position+spawnRange,Quaternion.identity);
		}
	}




}
